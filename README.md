# Random name picker #

Stress no more when choosing for a volunteer in an adhoc task force.
![Random name picker sample screenshot](/assets/images/RNGNamePickerCapture.PNG)

### What is this software is for and its feature ? ###

* Easy and simple to shift awkward situation by empowering Random Number Generator to choose the name.
* Useful being objective without human emotions.
* Just populate the namelist into the namelist/namelist.txt file.
* Names chosen will be placed into namelist/complete.txt file, to ensure the names in namelist.txt are used once.


### How do I get set up? ###

* Required softwares:
* [Python 3.7](https://www.python.org/downloads/release/python-370/)

### Disclaimer ###
* This is a software not intend for production environment.
* This software is not optimized to run with very large list.
* The RNG uses list that is loaded from files into memory for processing.


### Who do I talk to? ###

* [Chow Kar Meng's homepage](http://chow.karmeng.my)
* [Random Lists Generator](https://www.randomlists.com/random-names)