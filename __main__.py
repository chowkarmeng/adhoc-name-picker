import random
from pathlib import Path
import os

dirname = os.path.dirname(__file__)

def open_name_list():
    namelist = open(os.path.join(dirname, 'namelist\\namelist.txt')).readlines()
    return namelist

def open_complete_list():
    chkfile = Path(os.path.join(dirname, 'namelist\\complete.txt'))
    chkfile.touch(exist_ok=True)
    completelist = open(os.path.join(dirname,'namelist\\complete.txt')).readlines()
    return completelist

def random_pick(namelist):
    random_selected_name = random.choice(namelist)
    return random_selected_name


def append_name_to_file(selected_name):
    with open(os.path.join(dirname,'namelist\\complete.txt'), 'a+') as filename:
        filename.write(selected_name)
    filename.close()
    print("Added %s into assigned to minutes" % selected_name)

def diff_list(list1, list2):
    return list(set(list1) - set(list2)) + list(set(list2) - set(list1))

if __name__ == "__main__":
    namelist = open_name_list()
    difflist = open_complete_list()
    completelist = diff_list(namelist,difflist)
    print("Complete list: %s" % completelist)
    while True:
        random_select = random_pick(completelist)
        print("Randomly selected name: %s" % random_select)
        print("Randomly pick again ?[y/n]")
        user_choice = input()
        if user_choice == 'n' or user_choice == 'N':
            break
    append_name_to_file(random_select)

